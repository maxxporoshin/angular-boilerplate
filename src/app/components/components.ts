import { AppComponent } from '../app.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';

export const components = [
  AppComponent,
  HeaderComponent,
  FooterComponent
];
