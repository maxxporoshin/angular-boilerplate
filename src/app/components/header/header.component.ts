import { Component } from '@angular/core';

@Component({
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.sass'],
  selector: 'header-component'
})
export class HeaderComponent { }
