import { Component } from '@angular/core';

@Component({
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.sass'],
  selector: 'footer-component'
})
export class FooterComponent { }
