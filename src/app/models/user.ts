export class User {
  id: number;
  email: string;
  firstName: string;
  lastName: string;

  constructor(data: any) {
    if (data.hasOwnProperty('id')) {
      this.id = data.id;
    }

    if (data.hasOwnProperty('email')) {
      this.email = data.email;
    }

    if (data.hasOwnProperty('firstName')) {
      this.firstName = data.firstName;
    }

    if (data.hasOwnProperty('lastName')) {
      this.lastName = data.lastName;
    }
  }
}
