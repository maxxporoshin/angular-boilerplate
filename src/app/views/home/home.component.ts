import { Component, OnInit } from '@angular/core';

import { User } from '../../models/user';
import { UserService } from '../../services/user.service';

@Component({
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.sass']
})
export class HomeComponent implements OnInit {
  users: User[];

  constructor(
    public userService: UserService,
  ) { }

  ngOnInit() {
    this.userService.fetchUsers()
      .subscribe((users: User[]) => {
        this.users = users;
      });
  }
}
