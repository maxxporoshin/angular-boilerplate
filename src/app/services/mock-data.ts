export const users: any[] = [
  {
    id: 1,
    email: 'qwe@gmail.com',
    firstName: 'Egor',
    lastName: 'Woof'
  },
  {
    id: 2,
    email: 'rty@gmail.com',
    firstName: 'Sasha',
    lastName: 'Meow'
  },
  {
    id: 3,
    email: 'uio@gmail.com',
    firstName: 'Olya',
    lastName: 'Crya'
  },
  {
    id: 4,
    email: 'asd@gmail.com',
    firstName: 'Anya',
    lastName: 'Gav'
  },
  {
    id: 5,
    email: 'gfh@gmail.com',
    firstName: 'Kostya',
    lastName: 'Fur'
  }
];
