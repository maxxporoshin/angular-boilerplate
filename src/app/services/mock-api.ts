import { Observable } from 'rxjs/Observable';
import { Observer } from 'rxjs/Observer';

import { ApiResponse } from './api-response';

import { users } from './mock-data';

class Response implements ApiResponse {
  ok = true;
  status = 200;
  data: any = {};

  constructor(status, data) {
    this.status = status;
    this.data = data;
    this.ok = status < 300 ? true : false;
  }
}

export class MockApi {
  static getUsers(): Observable<Response> {
    return new Observable((observer: Observer<Response>) => {
      observer.next(new Response(200, { users }));
      observer.complete();
    });
  }
}
