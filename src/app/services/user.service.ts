import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { User } from '../models/user';

import { MockApi } from './mock-api';

@Injectable()
export class UserService {
  fetchUsers(): Observable<User[]> {
    return MockApi.getUsers().map(e => e.data.users.map(data => new User(data)));
  }
}
