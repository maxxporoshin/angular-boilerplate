export interface ApiResponse {
  ok: boolean,
  status: number,
  data: any
}
