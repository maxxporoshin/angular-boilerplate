import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { RoutingModule } from './routing.module';
import { AppComponent } from './app.component';

import { components } from './components/components';
import { views } from './views/views';
import { services } from './services/services';

import '../styles.sass';

@NgModule({
  declarations: [
    AppComponent,
    ...components,
    ...views
  ],
  imports: [
    BrowserModule,
    RoutingModule
  ],
  providers: services,
  bootstrap: [AppComponent]
})
export class AppModule { }
