import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app/app.module';

declare const ENVIRONMENT: string;

if (ENVIRONMENT === 'production') {
  enableProdMode();
}

platformBrowserDynamic().bootstrapModule(AppModule);
