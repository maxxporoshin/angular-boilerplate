const express = require('express');
const app = express();
const path = require('path');

let server;

switch(process.env.NODE_ENV) {
  case 'production':
    server = {
      host: '127.0.0.1',
      port: '4000'
    };
    break;
  case 'development':
  default:
    server = {
      host: '127.0.0.1',
      port: '3000'
    }
}

const dist = {
  dir: 'dist',
  index: 'index.html'
};

app.use(express.static(dist.dir));

app.use((req, res) => {
  res.sendFile(path.join(__dirname, `${dist.dir}${path.sep}${dist.index}`));
});

app.listen(server.port, server.host, () => {
  console.log(`Express server listens on ${server.host}:${server.port}`);
});
