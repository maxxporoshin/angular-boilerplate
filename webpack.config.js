const path = require('path');
const webpack = require('webpack');
const HtmlPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

function root(__path) {
  return path.join(__dirname, __path);
}

const envs = {
  development: {
    devtool: 'cheap-module-eval-source-map',
    uglify: false
  },
  production: {
    devtool: '',
    uglify: true
  }
};

let env = envs[process.env.NODE_ENV] || envs.development;

const config = {
  entry: {
    polyfills: './src/polyfills.ts',
    vendor: './src/vendor.ts',
    app: './src/main.ts'
  },
  output: {
    filename: '[name].js',
    path: root('./dist')
  },
  resolve: {
    extensions: ['.ts', '.js'],
    alias: {
      styles: root('./src/styles.sass'),
    }
  },
  module: {
    rules: [
      {
        test: /\.ts$/,
        loaders: [
          {
            loader: 'awesome-typescript-loader',
            options: { configFileName: root('./tsconfig.json'), sourceMap: !!env.devtool }
          },
          'angular2-template-loader'
        ]
      },
      {
        test: /\.html$/,
        loader: 'html-loader'
      },
      {
        test: /\.(png|jpe?g|gif|svg|woff|woff2|ttf|eot|ico)$/,
        loader: 'file-loader?name=',
        options: {
          name: 'assets/[name]-[hash:8].[ext]'
        }
      },
      {
        test: /\.s[ac]ss$/,
        exclude: root('./src/app'),
        loader: ExtractTextPlugin.extract({
          use: ['css-loader', 'sass-loader'],
          fallback: 'style-loader'
        })
      },
      {
        test: /\.s[ac]ss$/,
        include: root('./src/app'),
        loaders: ['raw-loader', 'sass-loader']
      }
    ]
  },
  plugins: [
    new webpack.ContextReplacementPlugin(
      /angular(\\|\/)core/,
      root('./src'),
      { }
    ),
    new webpack.optimize.CommonsChunkPlugin({
      name: ['app', 'vendor', 'polyfills']
    }),
    new HtmlPlugin({
      template: 'src/index.html',
      favicon: 'src/favicon.ico'
    }),
    new ExtractTextPlugin('styles.css'),
    new webpack.DefinePlugin({
      ENVIRONMENT: JSON.stringify(process.env.NODE_ENV || 'development')
    })
  ],
  devtool: env.devtool
};

if(env.uglify) {
  config.plugins.push(new webpack.optimize.UglifyJsPlugin({
    compress: {
      screw_ie8: true
    }
  }));
}

module.exports = config;
